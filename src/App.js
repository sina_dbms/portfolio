import React, {Component} from 'react';
import './App.css';
import Footer from './components/footer'
import NavigationBar from "./components/navigationBar";
import Content from "./components/content";

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {currentPage: window.location.pathname};
        this.updateCurrentPage = this.updateCurrentPage.bind(this);
    }

    updateCurrentPage(currentPageName) {
        this.setState({currentPage: currentPageName});
    }

    render() {
        return (
            <div>
                <NavigationBar currentPage={this.state.currentPage} updateCurrentPage={this.updateCurrentPage}/>
                <div className="container-fluid">
                    <Content currentPage={this.state.currentPage}/>
                    <Footer/>
                </div>
            </div>
        );
    }
}

export default App;
