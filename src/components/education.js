import React, {Component} from 'react';

class Education extends Component {
    render() {
        return (
            <div>
                <div className="row">
                    <div className="col-3">
                        <p className="s-heading mt-2 text-muted">{this.props['from']} - {this.props['to']}</p>
                    </div>
                    <div className="col-9">
                        <h4 className="mt-0">
                            {this.props['major']}<br/>
                            <a href={this.props['uniWebsite']} target="_blank" rel="noopener noreferrer"
                               className="s-place-name ml-2 mt-2 text-muted">{this.props['uniName']}</a>
                            <a className="ml-2" target="_blank" rel="noopener noreferrer"
                               href={this.props['mapLocation']}>
                                <i className="fa fa-map-marker fa-lg" aria-hidden="true"/>
                            </a>
                        </h4>
                    </div>
                </div>
            </div>
        );
    }
}

export default Education;