import React from 'react';

export default () => (
    <footer className="s-footer text-light font-weight-lighter small">&copy; {new Date().getFullYear()} Sina
        Darian</footer>
);