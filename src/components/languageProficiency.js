import React, {Component} from 'react';

class LanguageProficiency extends Component {
    render() {
        return (
            <div className="row">
                <div className="col-3">
                    <p className="s-heading mt-2">{this.props['name']}</p>
                </div>
                <div className="col-9">
                    <p className="mt-2">{this.props['description']}</p>
                </div>
            </div>
        );
    }
}

export default LanguageProficiency;