import React, {Component} from 'react'
import Modal from "./modal";

class PortfolioCard extends Component {
    render() {
        return (
            <div className="s-portfolio-card bg-dark text-light card mr-5 mb-5">
                <img className="card-img-top px-1" src={this.props.image} alt="Card"/>
                <div className="card-body">
                    <h4 className="card-title">{this.props.title}</h4>
                    <p className="card-text text-justify ml-2">{this.props.description}</p>
                </div>
                <div className="card-footer">
                    <button type="button"
                            className="btn btn-outline-primary"
                            data-toggle="modal"
                            data-target={"#" + this.props.modalID}>
                        View details
                    </button>
                    <Modal modalID={this.props.modalID}
                           description={this.props.description}
                           detailedDescription={this.props['detailedDescription']}
                           links={this.props['links']}
                           imageGallerySource={this.props['imageGallerySource']}
                           title={this.props.title}/>
                </div>
            </div>
        );
    }
}

export default PortfolioCard;