import React from 'react';

export default (props) => (
    <div className="row">
        <div className="col-3"><p className="s-heading">{props['skillName']}</p></div>
        <div className="col-8 col-lg-5 mt-2">
            <div className="progress" style={{height: '5px'}}>
                <div className="s-progressbar progress-bar" style={{width: props['progress'] + '%'}}/>
            </div>
        </div>
    </div>
);