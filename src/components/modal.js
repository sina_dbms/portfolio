import React, {Component} from 'react'
import VerticalImageGallery from "./verticalImageGallery";

class Modal extends Component {
    render() {
        return (
            <div className="modal fade" id={this.props.modalID} tabIndex="-1" role="dialog" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered modal-lg" role="document"
                     style={{minHeight: '85vh'}}>
                    <div className="modal-content bg-dark" style={{minHeight: 'inherit', minWidth: 'inherit'}}>
                        <div className="modal-header">
                            <h5 className="modal-title">{this.props.title}</h5>
                        </div>
                        <div className="modal-body container">
                            <div className="row">
                                <div className="col-sm-8 text-justify">
                                    <p>{this.props['detailedDescription']}</p>
                                    {
                                        this.props['links'] !== undefined ?
                                            <div>
                                                <br/>
                                                <h5>Links</h5>
                                                {
                                                    this.props['links'].map(
                                                        (item) => <div>
                                                            <a className="ml-2"
                                                               target="_balnk"
                                                               href={item.link}>
                                                                {item.label}
                                                            </a>
                                                            <br/>
                                                        </div>
                                                    )
                                                }
                                            </div>
                                            : ""
                                    }
                                </div>
                                <div className="col-sm-4">
                                    <VerticalImageGallery
                                        verticalImageGalleryIDPrefix={this.props.modalID}
                                        images={this.props['imageGallerySource']}/>
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-outline-secondary" data-dismiss="modal">
                                Close
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Modal;