import React, {Component} from 'react';
import ContactEntry, {Icons} from "./contactEntry";

class Contact extends Component {
    render() {
        return (
            <div className="pt-5 mb-5">
                <h2 className="s-page-header">Contact Me</h2>
                <div className="pl-3">
                    <p>
                        I am open to new ideas, partnerships or being hired as an employee.
                    </p>
                    <ContactEntry cKey="Email" cValue="sina.barghidarian@udo.edu" icon={Icons.mail}
                                  url="mailto:sina.barghidarian@udo.edu?subject=We%20are%20interested%20in%20your%20profile"/>
                    <br/>
                    <ContactEntry cKey="LinkedIn" cValue="Sina Barghidarian" icon={Icons.linkedIn}
                                  url="https://www.linkedin.com/in/sina-barghidarian/"/>
                    <ContactEntry cKey="Instagram" cValue="Sina BD" icon={Icons.instagram}
                                  url="https://www.instagram.com/sina_bd/"/>
                    <ContactEntry cKey="Xing" cValue="Sina Barghidarian" icon={Icons.xing}
                                  url="https://www.xing.com/profile/Sina_Barghidarian"/>
                </div>
            </div>
        )
    }
}

export default Contact;