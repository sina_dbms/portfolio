import React from 'react';

export default () => (
    <div className="s-left-column">
        <img className="rounded-circle mt-md-5 mt-3" src="/profile-photo.jpg" alt="Sina Darian"/>
        <hr className="mx-auto bg-light"/>
        <h4>
            Sina Darian
            <br/>
            <small>Software Developer</small>
        </h4>
    </div>
);