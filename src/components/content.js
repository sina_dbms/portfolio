import React, {Component} from 'react';
import Resume from './resume'
import Contact from "./contact";
import Home from "./home.js";
import LeftColumn from "./leftColumn";
import {Switch, Route} from "react-router-dom";
import PageNotFound from "./pageNotFound";
import Portfolio from "./portfolio";

class Content extends Component {

    render() {
        return (
            <div className="row text-white mb-5">
                <div className="col-md-4 d-flex justify-content-center">
                    <LeftColumn/>
                </div>
                <div className="col-md-8 d-flex">
                    <Switch>
                        <Route exact path="/" component={Home}/>
                        <Route path="/home" component={Home}/>
                        <Route path="/resume" component={Resume}/>
                        <Route path="/portfolio" component={Portfolio}/>
                        <Route path="/contact" component={Contact}/>
                        <Route path="*" component={PageNotFound}/>
                    </Switch>
                </div>
            </div>
        );
    }


}

export default Content;