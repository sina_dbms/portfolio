import React, {Component} from 'react'
import ImageModal from "./imageModal";

class VerticalImageGallery extends Component {

    constructor(props) {
        super(props);
        this.state = {}
        this.childIDPrefix = this.props['verticalImageGalleryIDPrefix'] + "_imageModalID_";
        for (let i = 0; i < this.props['images'].length; ++i) {
            this.state[[this.childIDPrefix + i]] = {
                childModalIsVisible: false,
                childModalStyle: {display: "none"}
            };
        }
        this.state['currentModalImageIndex'] = null;
        this.handleChildModalState = this.handleChildModalState.bind(this);
        this.handleModalCurrentImage = this.handleModalCurrentImage.bind(this);
    }

    handleModalCurrentImage(currentIndex, nextIndex, currentModalID) {
        if (currentIndex === null && nextIndex === null) {
            // If the close button or the area outside of the modal image is clicked, this case handles it.
            this.setState({'currentModalImageIndex': null});
            this.handleChildModalState(currentModalID);
        } else if (currentIndex !== null && nextIndex === null) {
            // If there are no images open and an image in the gallery is clicked on, this case handles it.
            this.setState({'currentModalImageIndex': currentIndex});
            this.handleChildModalState(currentModalID);
        } else {
            // If the navigation buttons are clicked, this case should handle it.
            let len = this.props['images'].length;
            let currentIndexAfterUpdate = ((nextIndex % len) + len) % len
            this.setState({'currentModalImageIndex': currentIndexAfterUpdate});
            this.handleChildModalState(currentModalID);
            this.handleChildModalState(this.childIDPrefix + currentIndexAfterUpdate);
        }
    }

    handleChildModalState(childId) {
        if (this.state[childId].childModalIsVisible) {
            this.setState({
                [childId]: {
                    childModalIsVisible: false,
                    childModalStyle: {display: "none"}
                }
            });
        } else {
            this.setState({
                [childId]: {
                    childModalIsVisible: true,
                    childModalStyle: {display: "flex"}
                }
            });
        }
    }

    render() {
        return (
            <div className="shadow">
                <ul className="s-image-gallery list-group text-dark" style={{overflow: "auto", maxHeight: "75vh"}}>
                    {this.props['images'].map((imgAddress, index) => {
                        let itemID = this.childIDPrefix + index;
                        return (
                            <div>
                                <span
                                    className="shadow list-group-item list-group-item-action mr-5 mb-2"
                                >
                                    <img src={imgAddress['source']}
                                         alt="image"
                                         style={{maxWidth: "200px"}}
                                         onClick={() => {
                                             this.handleModalCurrentImage(index, null, itemID);
                                         }}
                                    />
                                </span>
                                <ImageModal
                                    handleModalCurrentImage={this.handleModalCurrentImage}
                                    modalStyle={this.state[itemID].childModalStyle}
                                    imageSource={imgAddress}
                                    currentImageIndex={this.state.currentModalImageIndex}
                                    modalID={itemID}
                                />
                            </div>);
                    })
                    }
                </ul>
            </div>
        );
    }
}

export default VerticalImageGallery;