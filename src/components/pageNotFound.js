import React from 'react';

export default () => (
    <div className="pt-5">
        <h2 className="s-page-header">Page not found! :-(</h2>
    </div>
);