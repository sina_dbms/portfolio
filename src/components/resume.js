import React, {Component} from 'react';
import Experience from "./experience";
import Education from "./education";
import LanguageProficiency from "./languageProficiency";
import Skill from "./skill";
import FreeStyleSection from "./freeStyleSection";
import Awards from './awards'

class Resume extends Component {
    render() {
        return (
            <div className="s-resume-block pt-5">
                <h2 className="s-page-header">Resume</h2>
                <br/>
                <h3 className="ml-2">Experience</h3>
                <hr/>
                <div className="pl-3">
                    <Experience companyName="Die Continentale Krankenversicherung a. G." from="March, 2020"
                                to="present"
                                role="System Developer"
                                description="My Tasks are currently oriented on building a prototypical pipeline using
                                Jenkins and introducing Docker for the daily needs of development."
                                companyWebsite="https://www.continentale.de/"
                                mapLocation="https://goo.gl/maps/pfhUgurgZHKnJPom8"
                                bulletPointList={['Docker', 'Jenkins']}/>
                    <Experience companyName="Die Continentale Krankenversicherung a. G." from="August, 2018"
                                to="December, 2019"
                                role="System Developer"
                                description="For about a year and a half I've worked in a Scrum Team as a Java Developer.
                                 Some of the technologies and areas of responsibilities worth mentioning were:"
                                companyWebsite="https://www.continentale.de/"
                                mapLocation="https://goo.gl/maps/pfhUgurgZHKnJPom8"
                                bulletPointList={['Docker', 'Extending an existing Framework for Frontend Testing using Selenium',
                                    'Frontend development', 'Backend development',
                                    'Managing application servers like JBoss and WebSphere']}/>
                </div>
                <h3 className="ml-2">Education</h3>
                <hr/>
                <div className="pl-3">
                    <Education from="April, 2018" to="present" major="M.Sc. in applied computer science"
                               uniName="TU Dortmund University" uniWebsite="https://www.tu-dortmund.de/"
                               mapLocation="https://goo.gl/maps/EPrkYTCWjPNFLjy3A"/>
                    <br/>
                    <Education from="September, 2012" to="March, 2016" major="B.Sc. in Software engineering"
                               uniName=" Imam Khomeini International University" uniWebsite="http://www.ikiu.ac.ir/"
                               mapLocation="https://goo.gl/maps/GLrCS2EkDmFMuVco7"/>
                </div>
                <h3 className="ml-2">Skills</h3>
                <hr/>
                <div className="pl-3">
                    <Skill skillName="Machine learning" progress="75"/>
                    <br/>
                    <Skill skillName="Java" progress="90"/>
                    <Skill skillName="Python" progress="85"/>
                    <Skill skillName="Docker" progress="90"/>
                    <Skill skillName="Groovy" progress="80"/>
                    <Skill skillName="HTML / CSS" progress="85"/>
                    <Skill skillName="Maven" progress="80"/>
                    <Skill skillName="Linux" progress="70"/>
                    <Skill skillName="Jenkins" progress="75"/>
                    <Skill skillName="Kubernetes" progress="65"/>
                    <Skill skillName="Git" progress="65"/>
                    <Skill skillName="React" progress="70"/>
                </div>
                <h3 className="ml-2">Language proficiency</h3>
                <hr/>
                <div className="pl-3">
                    <LanguageProficiency name="Persian" description="Native"/>
                    <LanguageProficiency name="German" description="Fluent"/>
                    <LanguageProficiency name="English" description="Fluent"/>
                </div>
                <h3 className="ml-2">Awards</h3>
                <hr/>
                <div className="pl-3">
                    <Awards date="October, 2018" name="Deutschlandstipendium"/>
                </div>
                <h3 className="ml-2">Hobbies</h3>
                <hr/>
                <div className="pl-3">
                    <FreeStyleSection name="Playing piano"/>
                </div>
            </div>
        )
    }
}

export default Resume;