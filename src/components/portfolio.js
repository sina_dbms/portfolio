import React, {Component} from 'react'
import PortfolioCard from "./portfolioCard";

class Portfolio extends Component {
    render() {
        return (
            <div className="pt-5 mb-5">
                <h2 className="s-page-header">Portfolio</h2>
                <div className="ml-3">
                    <p>Here are some of the projects that I've done.</p>
                </div>
                <br/>
                <div className="d-flex flex-wrap ml-4">
                    {
                        projects.map((project, index) =>
                            <PortfolioCard modalID={"modal_" + index}
                                           title={project.title}
                                           description={project.description}
                                           detailedDescription={project.detailedDescription}
                                           links={project.links}
                                           image={project.coverImageSource}
                                           imageGallerySource={project.imageGallerySource}
                            />
                        )
                    }
                </div>
            </div>
        );
    }
}

const projects = [
    {
        title: "Email Classification",
        description: "Using four machine learning algorithms a CSV dataset containing nearly 6,000 emails will be " +
            "classified into two classes: Spam emails and Ham(not Spam) emails. The Experiments are performed in " +
            "Azure ML Studio.",
        detailedDescription: "In order to classify emails into two groups, i.e. Spams and Hams, four different machine" +
            " learning algorithms are used, that is, Two-Class Support Vector Machine, Two-Class Bayes Point Machine, " +
            "Two-Class Averaged Perceptron and Two-Class Decision Forest. The dataset that is being used by the " +
            "algorithms is a CSV data containing almost 6,000 rows. Each row has two columns. One column is the text " +
            "of the email, and the other is the label of this email. The label is either 0 (Ham) or 1 (Spam). The " +
            "experiments are performed in Azure ML Studio. ",
        links: [
            {
                link: "https://gallery.cortanaintelligence.com/Experiment/Spam-or-Ham-Support-vector-machine",
                label: "Two-Class Support Vector Machine"
            },
            {
                link: "https://gallery.cortanaintelligence.com/Experiment/Spam-or-Ham-Decision-Forest",
                label: "Two-Class Decision Forest"
            },
            {
                link: "https://gallery.cortanaintelligence.com/Experiment/Spam-or-Ham-Averaged-Perceptron",
                label: "Two-Class Averaged Perceptron"
            },
            {
                link: "https://gallery.cortanaintelligence.com/Experiment/Spam-or-Ham-Bayes-Point-Machine",
                label: "Two-Class Bayes Point Machine"
            },
            {
                link: "https://www.kaggle.com/karthickveerakumar/spam-filter",
                label: "Dataset source on Kaggle"
            }
        ],
        coverImageSource: "azureMLStudio.png",
        imageGallerySource: [
            {
                source: "emailClassificationSVMModel.png",
                caption: "Workflow of model using Two-Class Support Vector Machine"
            },
            {
                source: "emailClassificationSVMResults.png",
                caption: "Results of Two-Class Support Vector Machine"
            },
            {
                source: "emailClassificationDecisionForestResults.png",
                caption: "Results of Two-Class Decision Tree"
            },
            {
                source: "emailClassificationAveragedPerceptronResults.png",
                caption: "Results of Two-Class Averaged Perceptron"
            },
            {
                source: "emailClassificationBayesPointMachineModel.png",
                caption: "Results of Two-Class Bayes Point Machine"
            }
        ]
    },
    {
        title: "Breast cancer detection",
        description: "In this example the use of four machine learning algorithms in predicting breast cancer " +
            "based upon medical records of almost 700 patients is illustrated. The Experiments are performed in Azure " +
            "ML Studio.",
        detailedDescription: "Similar to my other machine learning experiment i.e. Email Classification, is in this " +
            "experiment a dataset containing medical records of almost 700 patients using four machine learning " +
            "algorithms, that is, Two-Class Support Vector Machine, Two-Class Bayes Point Machine, Two-Class " +
            "Averaged Perceptron and Two-Class Decision Forest, in two classes divided. One class contains the " +
            "patients having breast cancer predicted by the algorithm, and the other contains the healthy ones. " +
            "Similarly, the experiments are performed in Azure ML Studio.",
        links: [
            {
                link: "https://gallery.cortanaintelligence.com/Experiment/Breast-cancer-prediction-Support-Vector-Machine",
                label: "Two-Class Support Vector Machine"
            },
            {
                link: "https://gallery.cortanaintelligence.com/Experiment/Breast-cancer-prediction-Decision-Forest",
                label: "Two-Class Decision Forest"
            },
            {
                link: "https://gallery.cortanaintelligence.com/Experiment/Breast-cancer-prediction-Averaged-Perceptron",
                label: "Two-Class Averaged Perceptron"
            },
            {
                link: "https://gallery.cortanaintelligence.com/Experiment/Breast-cancer-prediction-Bayes-point-machine",
                label: "Two-Class Bayes Point Machine"
            }
        ],
        coverImageSource: "breastCancerAwarenessLogo.png",
        imageGallerySource: [
            {
                source: "breastCancerDetectionSVMModel.png",
                caption: "Workflow of the modal using Two-Class Support Vector Machine"
            },
            {
                source: "breastCancerDetectionSVMResults.png",
                caption: "Results of Two-Class Support Vector Machine"
            },
            {
                source: "breastCancerDetectionDecisionForestResults.png",
                caption: "Results of Two-Class Decision Tree"
            },
            {
                source: "breastCancerDetectionAveragedPerceptronResults.png",
                caption: "Results of Two-Class Averaged Perceptron"
            },
            {
                source: "breastCancerDetectionBayesPointMachineModel.png",
                caption: "Results of Two-Class Bayes Point Machine"
            }
        ]
    }
]

export default Portfolio;