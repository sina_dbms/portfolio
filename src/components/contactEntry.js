import React, {Component} from 'react';

export const Icons = {
    mail: 'fa fa-envelope',
    linkedIn: 'fa fa-linkedin-square',
    instagram: 'fa fa-instagram',
    xing: 'fa fa-xing-square'
}

class ContactEntry extends Component {
    render() {
        return (
            <div className="row">
                <div className="s-heading col-4">
                    {this.props['cKey']}:
                </div>
                <div className="s-contact-entry col-8">
                    <a target={this.props['url'].includes('mailto') ? "" : "_blank"} href={this.props['url']}>
                        <i className={this.props['icon']}
                           aria-hidden="true"/> {this.props['cValue']}
                    </a>
                </div>
            </div>
        )
    }
}

export default ContactEntry;