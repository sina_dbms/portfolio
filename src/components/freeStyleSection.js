import React from 'react';

export default (props) => (
    <div className="row">
        <div className="col-3">
            <p className="s-heading mt-2">{props['name']}</p>
        </div>
        <div className="col-9 col-lg-6">
            <p className="mt-2 font-weight-bold">{props['description']}</p>
        </div>
    </div>
);