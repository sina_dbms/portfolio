import React, {Component} from 'react'

class ImageModal extends Component {

    render() {
        return (
            <div className="s-image-modal text-light"
                 style={this.props['modalStyle']}
                 onClick={() => {
                     this.props.handleModalCurrentImage(null, null, this.props['modalID']);
                 }}>
                <div className="s-image-modal-navigation-buttons"
                     onClick={(event => {
                         event.stopPropagation();
                         this.props.handleModalCurrentImage(this.props.currentImageIndex, this.props.currentImageIndex - 1, this.props['modalID']);

                     })}
                     style={{left: 0}}>
                    <span>❮</span>
                </div>
                <div className="bg-dark rounded" onClick={(event => {
                    event.stopPropagation();
                })}>
                    <img className="mx-2 mt-2 mb-1" src={this.props['imageSource']['source']}/>
                    <h6 className="text-center">{this.props['imageSource']['caption']}</h6>
                    <hr className="mx-auto bg-light"/>
                    <button className="btn btn-outline-secondary mb-3 mx-2"
                            onClick={() => {
                                this.props.handleModalCurrentImage(null, null, this.props['modalID']);
                            }}>Close
                    </button>
                </div>
                <div className="s-image-modal-navigation-buttons"
                     onClick={event => {
                         event.stopPropagation();
                         this.props.handleModalCurrentImage(this.props.currentImageIndex, this.props.currentImageIndex + 1, this.props['modalID']);
                     }}
                     style={{right: 0}}>
                    <span>❯</span>
                </div>
            </div>
        );
    }
}

export default ImageModal;