import React, {Component} from 'react';

export default (props) => (
    <div>
        <div className="row">
            <div className="col-3">
                <p className="s-heading mt-2 text-muted">{props['date']}</p>
            </div>
            <div className="col-9">
                <p className="mt-0">
                    {props['name']}
                </p>
            </div>
        </div>
    </div>
);