import React, {Component} from 'react';

class Experience extends Component {
    render() {
        return (
            <div className="row">
                <div className="col-3">
                    <p className="s-heading mt-2 text-muted">{this.props['from']} - {this.props['to']}</p>
                </div>
                <div className="col-9">
                    <h4 className="mt-0">
                        {this.props['role']}<br/>
                        <a href={this.props['companyWebsite']} target="_blank" rel="noopener noreferrer"
                           className=" s-place-name ml-2 mt-2 text-muted">{this.props['companyName']}
                        </a>
                        <a className=" ml-2" target=" _blank" rel=" noopener noreferrer"
                           href={this.props['mapLocation']}>
                            <i className=" fa fa-map-marker fa-lg" aria-hidden="true"/>
                        </a>
                    </h4>
                    <div className=" row">
                        <div className=" col-sm-12 col-lg-7">
                            <p className=" text-justify ml-3">{this.props['description']}</p>
                            <ul>
                                {this.props['bulletPointList'].map(item => <li>{item}</li>)}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Experience;