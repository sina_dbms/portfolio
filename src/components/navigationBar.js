import React, {Component} from 'react'
import {Link} from "react-router-dom";

class NavigationBar extends Component {
    constructor(props) {
        super(props);
        switch (props['currentPage']) {
            case '/resume':
                this.state = {
                    homeTabClass: "nav-link",
                    resumeTabClass: "nav-link s-active-tab font-weight-bold active",
                    portfolioTabClass: "nav-link",
                    contactClass: "nav-link"
                };
                break;
            case '/portfolio':
                this.state = {
                    homeTabClass: "nav-link",
                    resumeTabClass: "nav-link",
                    portfolioTabClass: "nav-link  s-active-tab font-weight-bold active",
                    contactClass: "nav-link"
                };
                break;
            case '/contact':
                this.state = {
                    homeTabClass: "nav-link",
                    resumeTabClass: "nav-link",
                    portfolioTabClass: "nav-link",
                    contactClass: "nav-link s-active-tab font-weight-bold active"
                };
                break;
            case '/home':
            case '/':
                this.state = {
                    homeTabClass: "nav-link s-active-tab font-weight-bold active",
                    resumeTabClass: "nav-link",
                    portfolioTabClass: "nav-link",
                    contactClass: "nav-link"
                };
                break;
            default:
                this.state = {
                    homeTabClass: "nav-link",
                    resumeTabClass: "nav-link",
                    portfolioTabClass: "nav-link",
                    contactClass: "nav-link"
                };
        }
    }

    toggleTab(event) {
        let tabName = event.target['id'];
        this.setState({homeTabClass: "nav-link " + (tabName === 'homeTab' ? "s-active-tab font-weight-bold active" : "")});
        this.setState({resumeTabClass: "nav-link " + (tabName === 'resumeTab' ? "s-active-tab font-weight-bold active" : "")});
        this.setState({portfolioTabClass: "nav-link " + (tabName === 'portfolioTab' ? "s-active-tab font-weight-bold active" : "")});
        this.setState({contactClass: "nav-link " + (tabName === 'contactMeTab' ? "s-active-tab font-weight-bold active" : "")});
    }

    render() {
        return (
            <nav className="s-navbar navbar navbar-expand-sm navbar-dark justify-content-end">
                <button className="navbar-toggler justify-content-end" type="button" data-toggle="collapse"
                        data-target="#collapsibleNavBar">
                    <span className="navbar-toggler-icon"/>
                </button>
                <div className="collapse navbar-collapse justify-content-center" id="collapsibleNavBar">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <Link id="homeTab" className={this.state.homeTabClass} onClick={event => {
                                this.toggleTab(event);
                                this.props.updateCurrentPage('/home');
                            }} to="/home">Home</Link>
                        </li>
                        <li className="nav-item">
                            <Link id="resumeTab" className={this.state.resumeTabClass} onClick={event => {
                                this.toggleTab(event);
                                this.props.updateCurrentPage('/resume');
                            }} to="/resume">Resume</Link>
                        </li>
                        <li className="nav-item">
                            <Link id="portfolioTab" className={this.state.portfolioTabClass} onClick={event => {
                                this.toggleTab(event);
                                this.props.updateCurrentPage('/portfolio');
                            }} to="/portfolio">Portfolio</Link>
                        </li>
                        <li className="nav-item">
                            <Link id="contactMeTab" className={this.state.contactClass} onClick={event => {
                                this.toggleTab(event);
                                this.props.updateCurrentPage('/contact');
                            }} to="/contact">Contact</Link>
                        </li>
                    </ul>
                </div>
            </nav>
        );
    }
}

export default NavigationBar;