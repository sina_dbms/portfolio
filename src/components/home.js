import React from 'react'

export default () => (
    <div className="pt-5">
        <h2 className="s-page-header">Home</h2>
        <div className="row pl-3">
            <div className="col-sm-12 col-lg-7">
                <p className="text-justify">
                    Hello world,<br/><br/>
                    welcome to my portfolio website. My name is Sina. I'm an enthusiastic software developer currently
                    living in Germany. Creating responsive web applications, building infrastructures
                    for deploying applications leveraging technologies like Kubernetes, Docker, Jenkins, etc. as well as
                    building machine learning solutions are my current focus in my career. My passion is to apply the
                    best practices that belong to the daily routine of an experienced software engineer to the field of
                    data science and machine learning. <br/>
                    In the past time I've gained a lot of insights and experiences in the industry as well
                    as in
                    the university. As the time goes by and as I do more projects I
                    update the contents of my website and add new fancy features to it. So don't forget to
                    come
                    around again.<br/><br/>

                    Best regards,<br/>
                    Sina
                </p>
            </div>
        </div>
    </div>
);