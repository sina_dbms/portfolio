const fs = require('fs');
let data = JSON.parse(fs.readFileSync("./npmAuditReport", "utf-8"));
let vulnerabilities = data['metadata']['vulnerabilities'];

console.log("NPM audit results:");
console.log(vulnerabilities);

// vulnerabilities['low'] + vulnerabilities['moderate'] + vulnerabilities['high'] + vulnerabilities['critical'] > 0
if (vulnerabilities['high'] + vulnerabilities['critical'] > 0) {
    console.log("Build not successful. See audit results");
    process.exit(1);
}